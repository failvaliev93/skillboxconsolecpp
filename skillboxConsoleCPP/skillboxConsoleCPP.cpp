﻿#include <iostream>
#include <ctime>
#include <ctime>
#include <chrono>
#include "Helpers.h"
#include <vector>
#include <math.h>
#include <string>

#pragma warning(disable : 4996)

/*Print Hello World!*/
void print() {
    std::cout << "Hello World!\n";
}


//*** 17.5 Домашняя работа ***
class Bank {
public: //обязателен
    Bank() {
        //offshore = new long(0);
        bills.reserve(1);
        bills.push_back(0);
    }

    Bank(const long _money) {
        bills.reserve(1);
        bills.push_back(_money);
    }

    Bank(const Bank& b) { // user-defined copy 
        std::copy(b.bills.begin(), b.bills.end(), std::back_inserter(this->bills));
    }

    Bank& operator= (const Bank other)  {   // user-defined copy assignment
        std::copy(other.bills.begin(), other.bills.end(), std::back_inserter(this->bills));
        return *this;
    }

    ~Bank() {/*good bye my money*/}

private:
    //long *offshore = nullptr;
    std::vector<int> bills; //счета

public:
    long getMoney(unsigned int bill) {
        if (!bills[bill]) return 0;
        return bills[bill];
    }

    long getMoney() {
        if (bills.empty()) return 0;
        return bills[0];
    }

    void addMoney(unsigned int bill, long _money) {
        if (bills[bill]) bills[bill] += _money;
    }

    void addMoney(long _money) {
        if (getCountOfBills()>0) bills[0] += _money;
    }

    /*Дополнить класс Vector public методом, который будет возвращать длину (модуль)
    вектора. Протестировать*/
    int getCountOfBills() {
        return bills.size();
    }
};

class Vector3 {
public:
    Vector3():
        x(0),
        y(0),
        z(0)
    {}

    Vector3(double _x, double _y, double _z) :
        x(_x),
        y(_y),
        z(_z)
    {}
    ~Vector3(){}

    void show() {
        std::cout << "x:" << x << "y:" << y << "z:" << z << '\n';
    }

    double mod() {
        return sqrt(x*x + y*y + z*z);
    }

private:
    double x;
    double y;
    double z;
};

//*** 19.5 Домашняя работа ***
class Animal {
public:
    Animal() {}
    ~Animal() {}
    virtual void voice() {} 
    Animal(const Animal& b) = delete;
    Animal& operator= (const Animal other) = delete;
};

class Cat : public Animal {
public:
    Cat(){}
    ~Cat(){}
    void voice() override {
        std::cout << "Cat::Miauu" << '\n';
    }
};

class Cow : public Animal {
public:
    Cow() {}
    ~Cow() {}
    void voice() override {
        std::cout << "Cow::Mu-uuuu" << '\n';
    }
};


//*** 18.5 Домашняя работа ***
//Односвязный список
template <typename T>
class Stack {
private:
    T *list;
    int currentSize;
    int maxSize;

public:
    Stack(const unsigned int maxSize) {
        list = new T[maxSize];
        this->currentSize = -1;
        this->maxSize = maxSize;
    }

    ~Stack() {
        delete[] list;
    }

    bool push(const T &data) {
        if (currentSize <= maxSize) {
            //if (currentSize != 0) ++currentSize;
            ++currentSize;

            list[currentSize] = data;
            //std::cout << '\n' << " " << currentSize;

            //if (currentSize == 0) ++currentSize; //для первого

            return true;
        }
        return false;
    }

    T pop() {
        if (currentSize > 0) {
            --currentSize;
            return list[currentSize+1];
        }
    }

    void show() const {
        //std::cout << '\n';
        for (int i = 0; i <= currentSize && i <= maxSize; ++i){
            std::cout << '\t' << list[i]; //(T)
        }
        std::cout << '\n';
    }
};


int main()
{
    setlocale(LC_ALL, "ru");

    {int x = 100;
    int y(x + 10);
    int mult = x * y;

    std::cout << "Hello World!\n";
    print();
    std::cout << x << " " << y << " " << std::endl;


    //*** 13.4 Домашняя работа ***
    std::cout << std::endl << "*** 13.4 Домашняя работа ***" << std::endl;
    std::cout << "sqrSum(x, y)=" << sqrSum(x, y) << std::endl;


    //*** 14.3 Домашняя работа ***
    std::cout << std::endl << "*** 14.3 Домашняя работа ***" << std::endl;
    std::string str = "string";
    std::cout << "std::string str=" << str << std::endl;
    std::cout << "std::string str[0]" << str[0] << std::endl;
    std::cout << "str размер=" << str.length() << std::endl;
    std::cout << "str последний символ=" << str[str.length() - 1] << std::endl; }


    //*** 15.4 Домашняя работа ***
    {//1 цикл, 1 условие
        std::cout << std::endl << "*** 15.4 Домашняя работа ***" << std::endl;
        printNum(10, true);
        printNum(10, false); }


    //*** 16.5 Домашняя работа ***
    {std::cout << std::endl << "*** 16.5 Домашняя работа ***" << std::endl;
    const int COUNT1 = 5;
    const int COUNT2 = COUNT1;
    int arrInt[COUNT1][COUNT2];
    std::cout << "размерности N= " << COUNT1 << std::endl;

    time_t rawtime;
    struct tm* timeinfo;
    time(&rawtime);                  // текущая дата, выраженная в секундах
    timeinfo = localtime(&rawtime);  // текущая дата, представленная в нормальной форме
    const int today = timeinfo->tm_mday;
    std::cout << "текущего числа календаря: " << today << std::endl;

    auto start = std::chrono::system_clock::now(); //время вычисления

    /*В главном исполняемом файле (файл в котором находится функция main)
    создать двумерный массив размерности NxN и заполнить его таким образом,
    чтобы элемент с индексами i и j был равен i + j. */
    for (int i = 0; i < COUNT1; ++i) {
        for (int j = 0; j < COUNT2; ++j) {
            arrInt[i][j] = i + j;
        }
    }

    /*Вывести этот массив в консоль. */
    for (int i = 0; i < COUNT1; ++i) {
        for (int j = 0; j < COUNT2; ++j) {
            std::cout << arrInt[i][j] << "\t";
        }
        std::cout << std::endl;
    }

    /*Вывести сумму элементов в строке массива, индекс которой равен остатку
    деления текущего числа календаря на N. (в двумерном массиве a[i][j],
    i — индекс строки). */
    int ost = today % COUNT1; //5%5=0
    int sum = 0;
    for (int i = 0; i < COUNT1; ++i) {
        for (int j = 0; j < COUNT2; ++j) {
            if (ost == j) sum += arrInt[i][j];
        }
    }
    std::cout << "сумма=" << sum << std::endl;

    //результат время вычисления:
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    std::cout << "результат время вычисления: " << elapsed_seconds.count() << "s\n"; }


    //*** 17.5 Домашняя работа ***
    {std::cout << std::endl << "*** 17.5 Домашняя работа ***" << std::endl;
    long cash = 1000000;
    Bank anyBank(cash);

    Bank myBank(anyBank);
    anyBank.addMoney(0, -cash);
    //myBank.addMoney(1000000);

    Bank newBank = myBank;
    myBank.addMoney(0, -cash);
    std::cout << "money1=" << anyBank.getMoney() << std::endl;
    std::cout << "money2=" << myBank.getMoney() << std::endl;
    std::cout << "money3=" << newBank.getMoney() << std::endl;

    /*Дополнить класс Vector public методом, который будет возвращать длину (модуль)
        вектора. Протестировать*/
    Vector3 vector(12.0, 10.0, 10.5);
    vector.show();
    std::cout << vector.mod() << std::endl;;
    }


    //*** 18.5 Домашняя работа ***
    {std::cout << std::endl << "*** 18.5 Домашняя работа ***" << std::endl;

    Stack<int> stack(6);
    stack.push(4);
    stack.push(8);
    stack.push(15);
    stack.push(16);
    stack.push(23);
    stack.push(42);
    stack.show();
    stack.pop();
    stack.show();

    Stack<float> fStack(2);
    fStack.push(0.1); }

    //*** 19.5 Домашняя работа ***
    std::cout << std::endl << "*** 19.5 Домашняя работа ***" << std::endl;
    const int animC = 2;

    Cat cat;
    Cow cow;

    Animal* animals[animC]{
        &cat,
        &cow
    };
    for (int i = 0; i < animC; i++) {
        animals[i]->voice();
    }   
}